const fs = require('node:fs');
const path = require('node:path');

const filenames = new Array(100000).fill(0).map((value, index) => {
  return path.join(__dirname, 'test', `${index}.txt`);
});

const filesToCreate = filenames.slice(0, 80000);

/**
 * @param { string } filename
 * @param { string } data
 */
async function appendFile(filename, data = '') {
  return fs.promises.appendFile(filename, data);
}

/** @param { string } filename */
async function checkFileExists(filename) {
  return fs.promises.access(filename, fs.constants.F_OK);
}

/** @param { string } filename */
async function rmFile(filename) {
  return fs.promises.unlink(filename);
}

module.exports = {
  filenames,
  filesToCreate,
  appendFile,
  checkFileExists,
  rmFile
};