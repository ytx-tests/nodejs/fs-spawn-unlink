const { filenames, filesToCreate, appendFile, checkFileExists, rmFile } = require('./files');

async function createAll() {
  await Promise.all(filesToCreate.map(async (filename) => appendFile(filename)));
  console.info(`Created ${filesToCreate.length} files successfully!`);
}

async function deleteAll() {
  const promises = filenames.map(async (filename) => {
    await checkFileExists(filename);
    await rmFile(filename);
  });

  await Promise.allSettled(promises);
  console.info('Deleted all files successfully!');
}

(async () => {
  try {
    await createAll();
    await deleteAll();
  } catch {
    console.error(error);
  }
})();
