const fs = require('node:fs');

const { filenames, filesToCreate } = require('./files');

function createAll() {
  let numSuccess = 0;

  filesToCreate.forEach((filename) => {
    try {
      fs.appendFileSync(filename, '');
      ++numSuccess;
    } catch {

    }
  });
  console.info(`Created ${numSuccess} files successfully!`);
}

async function deleteAll() {
  filenames.forEach((filename) => {
    try {
      fs.accessSync(filename, fs.constants.F_OK);
      fs.unlinkSync(filename);
    } catch {

    }
  });
  console.info('Deleted all files successfully!');
}

try {
  createAll();
  deleteAll();
} catch (error) {
  console.error(error);
}
