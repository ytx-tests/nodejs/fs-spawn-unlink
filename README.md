# Node.js fs spawn unlink

This repository is used to test spawn & unlink.

## Promise version
```bash
node index-promise.js
```
This script will create four files (`one`, `two`, `three`, `four`) in the `test` directory under the repository root.
* If the `test` directory is not created beforehand, the command above will fail.
* If created, then the files will be created successfully, and then deleted. You can see the information in the console window.