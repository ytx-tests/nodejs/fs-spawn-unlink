const { filenames, filesToCreate, appendFile, checkFileExists, rmFile } = require('./files');

async function createAll() {
  return Promise.all(filesToCreate.map(async (filename) => appendFile(filename)))
      .then(() => console.info(`Created ${filesToCreate.length} files successfully!`));
}

async function deleteAll() {
  return Promise.allSettled(filenames.map(async (filename) => {
    return checkFileExists(filename)
      .then(() => rmFile(filename));
  }))
      .then(() => console.info('Deleted all files successfully!'));
}

createAll()
    .then(() => {
      deleteAll()
    })
    .catch(() => {
      console.error(error);
    });